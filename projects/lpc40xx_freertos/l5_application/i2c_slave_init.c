#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include <stdint.h>
#include <stdio.h>

void i2c2__slave_init(uint8_t slave_address_to_respond_to) {
  const uint8_t AA_bit = (1 << 2);
  const uint8_t I2EN_bit = (1 << 6);
  LPC_I2C2->ADR0 = slave_address_to_respond_to;
  LPC_I2C2->MASK0 = 0;
  LPC_I2C2->CONSET = AA_bit | I2EN_bit;
}; // i2c2__slave_init()
