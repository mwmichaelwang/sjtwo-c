#include <stdbool.h>
#include <stdint.h>

#define BLOCK_TOTAL 8
#define PAGES_PER BLOCK 16
#define PAGE_SIZE 256 // bytes

typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

// Adesto Status Register - Byte 1
typedef union {
  uint8_t byte;
  struct {
    uint8_t rdy_bsy : 1; // [0] - Ready/Busy Status
    uint8_t wel : 1;     // [1] - Write Enable Latch Status
    uint8_t bp0 : 1;     // [2] - Block Protection
    uint8_t res1 : 1;    // [3] - Reserved for future use
    uint8_t wpp : 1;     // [4] - Write Protect (WP) Pin Status
    uint8_t epe : 1;     // [5] - Erase/Program Error
    uint8_t res2 : 1;    // [6] - Reserved for future use
    uint8_t bpl : 1;     // [7] - Block Protection Locked
  } __attribute__((packed));
} adesto_status_1_s;

// Adesto Status Register - Byte 2
typedef union {
  uint8_t byte;
  struct {
    uint8_t rdy_bsy : 1; // [0]   - Ready/Busy Status
    uint8_t res : 3;     // [3:1] - Reserved for future use
    uint8_t rste : 1;    // [4]   - Reset Enabled
  } __attribute__((packed));
} adesto_status_2_s;

void adesto_cs(void);
void adesto_ds(void);
adesto_flash_id_s adesto_read_signature(void);

// Extra Credit
void adesto_flash_send_address(uint32_t address);
void adesto_read_status_reg(adesto_status_1_s *s1, adesto_status_2_s *s2);
void adesto_erase_page(uint32_t address);
void adesto_read(uint32_t address, uint32_t size_byte);
void adesto_write(uint32_t address, uint32_t size_byte, uint8_t *buffer);
void adesto_write_enable(void);