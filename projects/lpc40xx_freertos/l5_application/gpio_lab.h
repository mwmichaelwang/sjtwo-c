#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;

void set_as_input(port_pin_s gpio);
void set_as_output(port_pin_s gpio);
void set_high(port_pin_s gpio);
void set_low(port_pin_s gpio);
void set(port_pin_s gpio, bool high);
bool get_level(port_pin_s gpio);
