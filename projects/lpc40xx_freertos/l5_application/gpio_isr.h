#pragma once

#include "gpio_lab.h"
#include <stddef.h>

typedef enum {
  GPIO_INTR_RISING_EDGE, // 0
  GPIO_INTR_FALLING_EDGE // 1

} gpio_interrupt_e;

typedef void (*function_pointer_t)(void);

void gpio_attach_interrupt(port_pin_s gpio, gpio_interrupt_e interrupt_type, function_pointer_t callback);
void gpio_interrupt_dispatcher(void);