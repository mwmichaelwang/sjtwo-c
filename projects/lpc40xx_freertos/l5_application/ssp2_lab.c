#include "ssp2_lab.h"
#include "clock.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include <stdio.h>

const port_pin_s ssp2_cs = {1, 10};
const port_pin_s ssp2_cs_mirror = {0, 6}; // Used for Logic Analyzer

void ssp2__init(uint32_t max_clock_mhz) {

  const uint32_t ssp2_power = (1 << 20);
  const uint8_t ssp2_sck = 0b100;
  const uint8_t ssp2_mosi = 0b100;
  const uint8_t ssp2_miso = 0b100;

  const uint8_t eight_bit_transfer = 0b0111;
  const uint8_t spi_frame_format = (0b00 << 4);
  const uint8_t ssp_enable = (0b1 << 1);

  uint32_t pclk_mhz;
  uint32_t divider;

  // Power
  LPC_SC->PCONP |= ssp2_power;

  // Select SSP pin modes
  LPC_IOCON->P1_0 &= ~(0b111); // Clear FUNC [3:0]
  LPC_IOCON->P1_0 |= ssp2_sck;

  LPC_IOCON->P1_1 &= ~(0b111); // Clear FUNC [3:0]
  LPC_IOCON->P1_1 |= ssp2_mosi;

  LPC_IOCON->P1_4 &= ~(0b111); // Clear FUNC [3:0]
  LPC_IOCON->P1_4 |= ssp2_miso;

  set_as_output(ssp2_cs);
  set_as_output(ssp2_cs_mirror);
  set_high(ssp2_cs);

  // Set up control registers (CRO and CR1)
  LPC_SSP2->CR0 &= ~(0b1111); // Clear Data Size Select - DSS [3:0]
  LPC_SSP2->CR0 |= eight_bit_transfer;

  LPC_SSP2->CR0 &= ~(0b11 << 4); // Clear Frame Format - FRF [5:4]
  LPC_SSP2->CR0 |= spi_frame_format;

  LPC_SSP2->CR1 = ssp_enable;

  // Set Clock Prescale Register
  pclk_mhz = clock__get_core_clock_hz() / 1000000UL; // 96 MHz
  pclk_mhz /= (((LPC_SSP2->CR0 >> 8) & 0xFF) + 1);   // PCLK / (SCR + 1), SCR [15:8]
  divider = pclk_mhz / max_clock_mhz;

  LPC_SSP2->CPSR = divider; // SSP clock prescale register
} // ssp2__init()

uint8_t ssp2__send_receive_byte(uint8_t data_out) {
  const uint32_t busy_bit = (1 << 4);

  LPC_SSP2->DR = data_out;

  // Wait for transfer to complete
  while (LPC_SSP2->SR & busy_bit) {
    ;
  }

  return LPC_SSP2->DR;
} // ssp2__exchange_byte()

void ssp2__enable() {
  set_low(ssp2_cs);
  set_low(ssp2_cs_mirror);
} // ssp2__enable()

void ssp2__disable() {
  set_high(ssp2_cs);
  set_high(ssp2_cs_mirror);
} // ssp2__disable()