  
 *  LED(L) / Switch(SW) Layout
 *  ---------------
 *  L3  L2  L1  L0
 *   O   O   O   O
 *   O   O   O   O
 *  SW3 SW2 SW1 SW0
 *  ---------------
 
 How to use board for ...
 
 main: 
 ----------------------------
 *  Press SW3 to blink LED3
 *  Press SW2 to blink LED2
 *  Press SW1 to blink LED1
 *  Press SW0 to blink LED0

 
 main_extra_credit:
 ----------------------------------------------------
 *  Press and hold SW3 for rotating LED animation to the right
 *  Press and hold SW2 for rotating LED animation to the left
 *  Press SW1 to speed up animation
 *  Press SW0 to slow down animation
