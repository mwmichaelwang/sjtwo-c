/**
 *
 * i2c write 0x00 0 - Turn LEDs off
 * i2c write 0x00 1 - LED animation (Rotate Left)
 * i2c write 0x00 2 - LED animation (Rotate Right)
 * i2c write 0x00 3 - LED animation (Blink)
 *
 * i2c write 0x01 1 - Speed up LED animation
 * i2c write 0x01 2 - Slow down LED animation
 *
 **/
#include "FreeRTOS.h"
#include "gpio_lab.h"
#include "i2c_slave_functions.h"
#include "i2c_slave_init.h"
#include "sj2_cli.h"
#include "task.h"
#include <stdio.h>
#include <time.h>

#define MEM_SIZE 256

static volatile uint8_t slave_memory[MEM_SIZE];

const uint8_t right = 3;
const uint8_t left = 0;
const int max_speed = 100;
const int min_speed = 1700;
const int default_speed = 700;
int speed = default_speed;

void led_off_task(void *task_parameter);
void led_rol_task(void *task_parameter);
void led_ror_task(void *task_parameter);
void led_blink_task(void *task_parameter);
void led_speed_task(void *task_parameter);

// LEDs
static const port_pin_s led3 = {1, 18};
static const port_pin_s led2 = {1, 24};
static const port_pin_s led1 = {1, 26};
static const port_pin_s led0 = {2, 3};

port_pin_s led[4] = {led0, led1, led2, led3};

void main(void) {

  // Initialize all 256 bytes of slave memory to 0.
  for (int i = 0; i < MEM_SIZE; i++) {
    slave_memory[i] = 0;
  }

  i2c2__slave_init(0xFE);

  xTaskCreate(led_off_task, "led off task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(led_rol_task, "led rol task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(led_ror_task, "led ror task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(led_blink_task, "led blink task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(led_speed_task, "led speed task", 1024, NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
} // main()

bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory) {
  if (memory_index < MEM_SIZE) {
    *memory = slave_memory[memory_index];
  }
  return memory_index < MEM_SIZE;
} // i2c_slave_callback__read_memory()

bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value) {
  if (memory_index < MEM_SIZE) {
    slave_memory[memory_index] = memory_value;
  }
  return memory_index < MEM_SIZE;
} // i2c_slave_callback__write_memory()

void led_off_task(void *task_parameter) {
  while (1) {
    if (slave_memory[0] == 0) {

      set_high(led[0]);
      set_high(led[1]);
      set_high(led[2]);
      set_high(led[3]);
    }
  }
} // led_off_task()

void led_rol_task(void *task_parameter) {
  uint8_t dir = left;
  while (1) {
    if (slave_memory[0] == 1) {
      set_low(led[dir]);
      vTaskDelay(speed);
      set_high(led[dir]);
      dir++;
      if (dir == 3) {
        set_low(led[dir]);
        vTaskDelay(speed);
        set_high(led[dir]);
        dir = left;
      }
    }
  }
} // led_rol_task()

void led_ror_task(void *task_parameter) {
  uint8_t dir = right;

  while (1) {
    if (slave_memory[0] == 2) {
      set_low(led[dir]);
      vTaskDelay(speed);
      set_high(led[dir]);
      dir--;
      if (dir == 0) {
        set_low(led[dir]);
        vTaskDelay(speed);
        set_high(led[dir]);
        dir = right;
      }
    }
  }
} // led_ror_task()

void led_blink_task(void *task_parameter) {
  while (1) {
    if (slave_memory[0] == 3) {
      set_high(led[0]);
      set_high(led[1]);
      set_high(led[2]);
      set_high(led[3]);
      vTaskDelay(speed);
      set_low(led[0]);
      set_low(led[1]);
      set_low(led[2]);
      set_low(led[3]);
      vTaskDelay(speed);
    }
  }
} // led_blink_task()

void led_speed_task(void *task_parameter) {
  while (1) {
    if (slave_memory[1] == 1) { // speed up
      slave_memory[1] = 0;
      if (speed != max_speed) {
        speed -= 200;
      }
    } else if (slave_memory[1] == 2) { // slow down
      slave_memory[1] = 0;
      if (speed != min_speed) {
        speed += 200;
      }
    }
    vTaskDelay(100);
  }
} // led_speed_task()
