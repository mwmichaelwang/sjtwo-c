#include "uart_lab.h"
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"
#include <stddef.h>
#include <stdio.h>

static QueueHandle_t uart_rx_queue;

static LPC_UART_TypeDef *uart_table[] = {LPC_UART2, LPC_UART3};

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {

  LPC_UART_TypeDef *UART = uart_table[uart];

  const uint32_t uart2_power = (1 << 24);
  const uint32_t uart3_power = (1 << 25);
  const uint8_t u2_txd = 0b010;
  const uint8_t u2_rxd = 0b010;
  const uint8_t u3_txd = 0b010;
  const uint8_t u3_rxd = 0b010;
  const uint16_t divisor_16_bit = peripheral_clock / (16 * baud_rate);
  const uint8_t dlab_bit = (1 << 7);
  const uint8_t char_len_8_bit = 0b11;
  const uint8_t fifo_enable = 0b1;

  switch (uart) {
  case UART_2:
    fprintf(stderr, "UART2 initialized!\n");
    LPC_SC->PCONP |= uart2_power;

    LPC_IOCON->P2_8 &= ~(0b111);
    LPC_IOCON->P2_8 |= u2_txd;

    LPC_IOCON->P2_9 &= ~(0b111);
    LPC_IOCON->P2_9 |= u2_rxd;
    break;

  case UART_3:
    fprintf(stderr, "UART3 initialized!\n");
    LPC_SC->PCONP |= uart3_power;

    LPC_IOCON->P4_28 &= ~(0b111);
    LPC_IOCON->P4_28 |= u3_txd;

    LPC_IOCON->P4_29 &= ~(0b111);
    LPC_IOCON->P4_29 |= u3_rxd;
    break;
  }

  // Setup DLL, DLM, FDR, LCR registers
  UART->LCR |= dlab_bit;
  UART->DLL = (divisor_16_bit >> 0) & 0xFF; // Lower 8 bits
  UART->DLM = (divisor_16_bit >> 8) & 0xFF; // Upper 8 bits
  UART->LCR &= ~dlab_bit;

  UART->LCR &= ~(0b11); // Clear Word Length Select - WLS [1:0]
  UART->LCR |= char_len_8_bit;

  UART->FDR = 0x10; // MULVAL [7:4] = 1, DIVADDVAL [3:0] = 0
  UART->FCR |= fifo_enable;
} // uart_lab__init()

static void receive_interrupt(void) {
  uart_iir_t iir;
  uart_lsr_t lsr;

  uint8_t interrupt_pending = 0;
  uint8_t receive_data_available = 0x2;
  uint8_t char_time_out = 0x6;

  iir.word = LPC_UART3->IIR;
  if (iir.INTSTATUS == interrupt_pending) {
    if ((iir.INTID == receive_data_available) || (iir.INTID == char_time_out)) {
      lsr.byte = LPC_UART3->LSR;
      if (lsr.RDR) {                      // Check if receiver FIFO has data...
        const char byte = LPC_UART3->RBR; // Copy data from RBR register to input_byte
        xQueueSendFromISR(uart_rx_queue, &byte, NULL);
      }
    }
  }
} // receive_interrupt()

void uart__enable_receive_interrupt(uart_number_e uart) {
  LPC_UART_TypeDef *UART = uart_table[uart];

  const uint8_t dlab_bit = (1 << 7);
  const uint8_t rbrie_bit = (1 << 0);

  if (uart == UART_3)
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART3, receive_interrupt, "Receive Interrupt");
  else
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, receive_interrupt, "Receive Interrupt");

  // Enable UART receive interrupt
  UART->LCR &= ~dlab_bit;
  UART->IER |= rbrie_bit;

  uart_rx_queue = xQueueCreate(1, sizeof(char));
}

bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {
  return xQueueReceive(uart_rx_queue, input_byte, timeout);
} // uart_lab__get_char_from_queue()

void uart_lab__polled_put(uart_number_e uart, char output_byte) {
  LPC_UART_TypeDef *UART = uart_table[uart];
  uart_lsr_t lsr;

  lsr.byte = UART->LSR;
  UART->THR = output_byte; // Copy output_byte to THR register

  while (!lsr.THRE) { // Wait for transfer to finish...
    lsr.byte = UART->LSR;
  }
} // uart_lab__polled_put()

bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  LPC_UART_TypeDef *UART = uart_table[uart];
  uart_lsr_t lsr;

  lsr.byte = UART->LSR;
  while (!lsr.RDR) { // Wait for data to become available...
    lsr.byte = UART->LSR;
  }

  *input_byte = UART->RBR; // Copy data from RBR register to input_byte

  // Check for error in receiving FIFO
  lsr.byte = UART->LSR;
  if (lsr.RXFE) {
    return true; // Error found!
  }
  return false;
} // uart_lab__polled_get()