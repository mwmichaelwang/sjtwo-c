#include "gpio_isr.h"
#include "lpc40xx.h"

#define num_ports 2
#define num_pins 32

function_pointer_t callback_rising_table[num_ports][num_pins] = {NULL};  // Lookup table for rising edge callbacks
function_pointer_t callback_falling_table[num_ports][num_pins] = {NULL}; // Lookup table for falling edge callbacks

void gpio_attach_interrupt(port_pin_s gpio, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  uint8_t port = 0;
  // Lookup table for interrupt enabling on rising/falling edge
  volatile uint32_t *gpioint_en_table[2][2] = {[0][0] = &(LPC_GPIOINT->IO0IntEnR),
                                               [0][1] = &(LPC_GPIOINT->IO0IntEnF),
                                               [1][0] = &(LPC_GPIOINT->IO2IntEnR),
                                               [1][1] = &(LPC_GPIOINT->IO2IntEnF)};

  if (gpio.port == 0)
    port = 0;

  else if (gpio.port == 2)
    port = 1;

  // Place callback into lookup table based on interrupt type
  if (interrupt_type == GPIO_INTR_RISING_EDGE) {
    callback_rising_table[port][gpio.pin] = callback; // Rising edge
  } else {
    callback_falling_table[port][gpio.pin] = callback; // Falling edge
  }
  *(gpioint_en_table[port][interrupt_type]) |=
      (1 << gpio.pin); // Configuring register to trigger on rising/falling edge

} // gpio_attach_interrupt()

void gpio_interrupt_dispatcher(void) {
  uint8_t port = 0;
  port = (LPC_GPIOINT->IntStatus & 1) ? 0 : 1; // Find which port generated the interrupt
                                               // Bit 0 = 1: At least one pending interrupt on Port 0
                                               // Bit 2 = 1: At least one pending interrupt on Port 2

  // Lookup table for interrupt status on rising/falling edge and interrupt clearing
  // Interrupt status registers are defined with 'read only' permissions (__I) and should not be altered
  volatile uint32_t *gpioint_stat_table[2][3] = {[0][0] = (uint32_t *const) & (LPC_GPIOINT->IO0IntStatR),
                                                 [0][1] = (uint32_t *const) & (LPC_GPIOINT->IO0IntStatF),
                                                 [0][2] = &(LPC_GPIOINT->IO0IntClr),
                                                 [1][0] = (uint32_t *const) & (LPC_GPIOINT->IO2IntStatR),
                                                 [1][1] = (uint32_t *const) & (LPC_GPIOINT->IO2IntStatF),
                                                 [1][2] = &(LPC_GPIOINT->IO2IntClr)};

  // Find which pin generated the interrupt
  for (uint8_t i = 0; i < 32; i++) {
    if (*gpioint_stat_table[port][0] & (1 << i)) {  // Check if it is rising edge enabled
      if (callback_rising_table[port][i] != NULL) { // Check function callback has been attached
        callback_rising_table[port][i]();           // Invoke user registered callback
      }
    }

    if (*gpioint_stat_table[port][1] & (1 << i)) {   // Check if it is falling edge enabled
      if (callback_falling_table[port][i] != NULL) { // Check function callback has been attached
        callback_falling_table[port][i]();           // Invoke user registered callback
      }
    }
    (*gpioint_stat_table[port][2]) |= (1 << i); // Clear interrupt
  }
} // gpio_interrupt_dispatcher()