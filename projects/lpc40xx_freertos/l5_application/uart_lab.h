#pragma once

#include "lpc40xx.h"
#include <stdbool.h>
#include <stdint.h>

typedef enum {
  UART_2,
  UART_3,
} uart_number_e;

// UARTn Line Status Register
typedef union {
  uint8_t byte;
  struct {
    uint8_t RDR : 1;
    uint8_t OE : 1;
    uint8_t PE : 1;
    uint8_t FE : 1;
    uint8_t BI : 1;
    uint8_t THRE : 1;
    uint8_t TEMT : 1;
    uint8_t RXFE : 1;
  } __attribute__((packed));
} uart_lsr_t;

// UARTn Interrupt Identification Register
typedef union {
  uint32_t word;
  struct {
    uint32_t INTSTATUS : 1;
    uint32_t INTID : 3;
    uint32_t RES : 2;
    uint32_t FIFOENABLE : 2;
    uint32_t ABEOINT : 1;
    uint32_t ABTOINT : 1;
  } __attribute__((packed));
} uart_iir_t;

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate);
void uart__enable_receive_interrupt(uart_number_e uart_number);
bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout);
bool uart_lab__polled_get(uart_number_e uart, char *input_byte);
void uart_lab__polled_put(uart_number_e uart, char output_byte);
