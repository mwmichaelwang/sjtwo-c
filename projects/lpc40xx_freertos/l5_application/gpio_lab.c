#include "gpio_lab.h"
#include "lpc40xx.h"
#include <stdbool.h>
#include <stdint.h>

static LPC_GPIO_TypeDef *gpio_table[] = {LPC_GPIO0, LPC_GPIO1, LPC_GPIO2,
                                         LPC_GPIO3, LPC_GPIO4, LPC_GPIO5}; // Look up table for all GPIO ports

void set_as_input(port_pin_s gpio) {
  LPC_GPIO_TypeDef *g = gpio_table[gpio.port];
  g->DIR &= ~(1 << gpio.pin); // 0 = input
} // set_as_input()

void set_as_output(port_pin_s gpio) {
  LPC_GPIO_TypeDef *g = gpio_table[gpio.port];
  g->DIR |= (1 << gpio.pin); // 1 = input
} // set_as_output()

void set_high(port_pin_s gpio) {
  LPC_GPIO_TypeDef *g = gpio_table[gpio.port];
  g->MASK &= ~(1 << gpio.pin);
  g->SET = (1 << gpio.pin);
} // set_high()

void set_low(port_pin_s gpio) {
  LPC_GPIO_TypeDef *g = gpio_table[gpio.port];
  g->MASK &= ~(1 << gpio.pin);
  g->CLR = (1 << gpio.pin);
} // set_low()

// {bool} high - true => set pin high, false => set pin low
void set(port_pin_s gpio, bool high) {
  if (high)
    set_high(gpio);
  else
    set_low(gpio);
} // set()

// {bool} pin level high => true, low => false
bool get_level(port_pin_s gpio) {
  LPC_GPIO_TypeDef *g = gpio_table[gpio.port];
  return ((g->PIN & (1 << gpio.pin)) != 0);
} // get_level()
