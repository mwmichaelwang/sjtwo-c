#include <stdint.h>

// Part 0
void ssp2__init(uint32_t max_clock_mhz);
uint8_t ssp2__send_receive_byte(uint8_t data_out);
void ssp2__enable();
void ssp2__disable();