#include "adesto.h"
#include "ssp2_lab.h"
#include <stdio.h>

// Command Opcodes
const uint8_t read_id_opcode = 0x9F;
const uint8_t read_array = 0x0B;
const uint8_t page_erase = 0x81;
const uint8_t page_program = 0x02;
const uint8_t write_enable = 0x06;
const uint8_t write_disable = 0x04;
const uint8_t read_status_reg = 0x05;

const uint8_t dummy_byte = 0xFF; // Used to receive data

// Functions
void adesto_cs(void) { ssp2__enable(); } // adesto_cs()

void adesto_ds(void) { ssp2__disable(); } // adesto_ds()

adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s id;

  adesto_cs();
  {
    ssp2__send_receive_byte(read_id_opcode);

    id.manufacturer_id = ssp2__send_receive_byte(dummy_byte);
    id.device_id_1 = ssp2__send_receive_byte(dummy_byte);
    id.device_id_2 = ssp2__send_receive_byte(dummy_byte);
    id.extended_device_id = ssp2__send_receive_byte(dummy_byte);
  }
  adesto_ds();

  return id;
} // adesto_read_signature()

// Sending 24-bit address 1 byte at a time with MSB first.
void adesto_flash_send_address(uint32_t address) {
  ssp2__send_receive_byte((address >> 16) & 0xFF);
  ssp2__send_receive_byte((address >> 8) & 0xFF);
  ssp2__send_receive_byte((address >> 0) & 0xFF);
} // adesto_flash_send_address()

void adesto_read_status_reg(adesto_status_1_s *s1, adesto_status_2_s *s2) {

  adesto_cs();
  {
    ssp2__send_receive_byte(read_status_reg);
    s1->byte = ssp2__send_receive_byte(dummy_byte);
    s2->byte = ssp2__send_receive_byte(dummy_byte);
  }
  adesto_ds();

} // adesto_read_status_reg()

void adesto_erase_page(uint32_t address) {
  adesto_status_1_s s1;
  adesto_status_2_s s2;

  fprintf(stderr, "Page Erase\n");
  fprintf(stderr, "-------------------------------------------------\n");

  adesto_write_enable();

  adesto_cs();
  {
    ssp2__send_receive_byte(page_erase);
    adesto_flash_send_address(address);
  }
  adesto_ds();

  // Wait for page erase to complete
  while (1) {
    adesto_read_status_reg(&s1, &s2);
    if (s1.rdy_bsy == 0) {
      break; // Device is ready, continue...
    }
  }
  fprintf(stderr, "Page Erase %s\n", (s1.epe ? "failed!\n" : "succeeded!\n")); // 1 - Error, 0 - Success
}

void adesto_read(uint32_t address, uint32_t size_byte) {
  uint32_t i;
  uint8_t buffer[PAGE_SIZE];

  fprintf(stderr, "\nPage Read\n");
  fprintf(stderr, "-------------------------------------------------");
  adesto_cs();
  {
    ssp2__send_receive_byte(read_array);
    adesto_flash_send_address(address);
    ssp2__send_receive_byte(dummy_byte); // "DON'T CARE" byte

    // Read byte(s) from memory
    for (i = 0; i < size_byte; i++) {
      buffer[i] = ssp2__send_receive_byte(dummy_byte);
      if (i % 8 == 0)
        fprintf(stderr, "\n");
      fprintf(stderr, "%02X  ", buffer[i]);
    }
    fprintf(stderr, "\n");
  }
  adesto_ds();
} // adesto_read()

void adesto_write(uint32_t address, uint32_t size_byte, uint8_t *buffer) {
  adesto_status_1_s s1;
  adesto_status_2_s s2;
  uint32_t i;

  fprintf(stderr, "\nPage Write\n");
  fprintf(stderr, "-------------------------------------------------\n");

  adesto_write_enable();

  adesto_cs();
  {
    ssp2__send_receive_byte(page_program);
    adesto_flash_send_address(address);

    // Write data into memory
    for (i = 0; i < size_byte; i++) {
      ssp2__send_receive_byte(buffer[i]);
    }
  }
  adesto_ds();

  // Wait for page write to complete
  while (1) {
    adesto_read_status_reg(&s1, &s2);
    if (s1.rdy_bsy == 0) {
      break; // Device is ready, continue...
    }
  }

  fprintf(stderr, "Page Write %s\n", (s1.epe ? "failed!\n" : "succeeded!\n")); // 1 - Error, 0 - Success
} // adesto_write()

void adesto_write_enable(void) {
  fprintf(stderr, "Write Enabled\n");
  adesto_cs();
  { ssp2__send_receive_byte(write_enable); }
  adesto_ds();
} // adesto_write_enable()